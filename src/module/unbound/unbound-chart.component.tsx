import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

interface UnboundChartComponent{
    data:Array<{date:string,unboundAmount:number}>
}

export const UnboundChartComponent=(props:UnboundChartComponent)=>{
    const {data}=props
    return(
        <ResponsiveContainer width="100%" height={300}>
            <LineChart
                width={500}
                height={300}
                data={data}
                margin={{
                    top: 5,
                    right: 30,
                    left: 20,
                    bottom: 5,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="date"/>
                <YAxis/>
                <Tooltip />
                <Legend verticalAlign={"top"}/>
                <Line name={"Unbounded $JUNO"} type="monotone" dataKey="unboundAmount" stroke="#8884d8" activeDot={{ r: 8 }} />
            </LineChart>
        </ResponsiveContainer>
    )
}