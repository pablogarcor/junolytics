import axios from "axios";
import {addDays,format} from "date-fns"

export const getValidators = async ()=>{
    try {
        let nextKey=undefined
        let validator: any[]=[]
        do{
            // @ts-ignore
            let res = await axios.get(
                `https://juno-lcd.stakely.io/cosmos/staking/v1beta1/validators`,
                {
                    params:{'pagination.key':nextKey},
                }
                )

            nextKey=res.data.pagination.next_key
            Array.prototype.push.apply(validator, res.data.validators);
        }while (nextKey!==null)
        return validator
    }catch (e) {
        return []
    }
}

export const getUnbondingByValidatorAddr = async (validatorAddr:string)=>{
    try {
        let nextKey=null
        let unbonding: any[]=[]
        do{
            // @ts-ignore
            let res = await axios.get(
                `https://juno-lcd.stakely.io/cosmos/staking/v1beta1/validators/${validatorAddr}/unbonding_delegations`,
                {
                    params:{'pagination.key':nextKey},
                }
            )

            nextKey=res.data.pagination.next_key
            Array.prototype.push.apply(unbonding, res.data.unbonding_responses);
        }while (nextKey!==null)
        return unbonding
    }catch (e) {
        return []
    }
}

export const getUnbondingAllValidators = async ()=>{
    try {
        const validators=await getValidators()
        const unbondingByByValidator=  await Promise.all(
            validators.map(async (validator)=>{
                return await getUnbondingByValidatorAddr(validator.operator_address)
            })
        )
        const dateArray=[...new Array(28)]
        return   dateArray.map((dateItem,index)=>{
            const date=addDays(new Date(),index)
            const formattedDate=format(date,'MM-dd')
            const unboundAmount=  unbondingByByValidator.reduce((accBalance, currentValidator)=>{
                return accBalance + currentValidator.reduce((accBalance: any, currentDelegator: any[])=>{
                    //@ts-ignore
                    return accBalance + currentDelegator.entries.reduce((accBalance: any, currentEntry: { completion_time: string | string[]; balance: any; })=>{
                        if(currentEntry.completion_time.includes(formattedDate)){
                            return parseInt(currentEntry.balance)+accBalance
                        }else{
                            return accBalance
                        }
                    },0)
                },0)
            },0)
            return {date:formattedDate,unboundAmount:unboundAmount/1000000}
        })
    }catch (e){
        return []
    }
}