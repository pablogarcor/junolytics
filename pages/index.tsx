import type { NextPage } from 'next'
import {Backdrop, CircularProgress, Grid, Typography} from "@mui/material";
import {useEffect, useState} from "react";
import {getUnbondingAllValidators} from "../src/module/unbound/unbound.service";
import {UnboundChartComponent} from "../src/module/unbound/unbound-chart.component";

const Home: NextPage = () => {
  const [chartData,setChartData]=useState([{date:'',unboundAmount:0}])
  const [loading,setLoading]=useState(false)
  useEffect(() => {
    setLoading(true)
    getUnbondingAllValidators()
        .then(response=>setChartData(response))
        .finally(()=>setLoading(false))
  }, [])
  return (
      <>
        <Backdrop open={loading}>
          <CircularProgress color='inherit' />
        </Backdrop>
        <Grid
            container
            direction={"column"}
            alignItems={"center"}
            rowSpacing={4}
        >
          <Grid
              item
          >
            <Typography variant={"h3"}>
              Unbounded $JUNO in the next 28 days
            </Typography>
          </Grid>
          <Grid
              item
              sx={{
                width:'100%'
              }}
          >
            <UnboundChartComponent data={chartData}/>
          </Grid>
        </Grid>
      </>
  )
}

export default Home
